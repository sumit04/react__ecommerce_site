import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logo from '../logo.svg';
import styled from 'styled-components';
import { ButtonContainer } from './Button';

class NavBar extends Component {
  render() {
    return (
      <NavWrapper className='navbar navbar-expand-sm bg-dark navbar-dark px-sm-5'>
        <Link to='/'>
          <img src={logo} alt='store' className='navbar-brand' />
        </Link>
        <ul className='navbar-nav align-items-center'>
          <li className='nav-item ml-5'>
            <Link to='/' className='nav-link'>
              Products
            </Link>
          </li>
        </ul>
        <Link to='/cart' className='ml-auto'>
          <ButtonContainer>
            <i className='fas fa-cart-plus mr-2' />
            My cart
          </ButtonContainer>
        </Link>
      </NavWrapper>
    );
  }
}

const NavWrapper = styled.nav`
  .nav-link {
    color: var(--mainYellow) !important;
    font-size: 1.3rem;
    text-transform: capitalizes;
  }
`;

export default NavBar;
